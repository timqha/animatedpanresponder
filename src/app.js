/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  Animated,
  PanResponder,
  Dimensions
} from 'react-native';

type Props = {};
export default class App extends Component<Props> {

  _keyExtractor = (item) => {
    return item.id.toString();
  }

  render() {
    const data = [
      {id: 1, title: "Title 1", content: 'content content content ', color: '#33be84'},
      {id: 2, title: "Title 2", content: 'content content content ', color: '#fbbe84'},
      {id: 3, title: "Title 3", content: 'content content content ', color: '#f85b9b'},
      {id: 4, title: "Title 4", content: 'content content content ', color: '#37c3f4'},
      {id: 5, title: "Title 5", content: 'content content content ', color: '#33be84'},
      {id: 6, title: "Title 6", content: 'content content content ', color: '#33be84'},
      {id: 7, title: "Title 7", content: 'content content content ', color: '#37c3f4'},
      {id: 8, title: "Title 8", content: 'content content content ', color: '#37c3f4'},
      {id: 9, title: "Title 9", content: 'content content content ', color: '#37c3f4'},
      {id: 10, title: "Title 10", content: 'content content content ', color: '#37c3f4'},
      {id: 11, title: "Title 11", content: 'content content content ', color: '#37c3f4'},
      {id: 12, title: "Title 12", content: 'content content content ', color: '#37c3f4'}
    ];
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          keyExtractor={this._keyExtractor}
          renderItem={({item, index}) => <ItemProduct title={item.title} content={item.content} color={item.color} /> }
        />
      </View>
    );
  }
}

class ItemProduct extends Component<Props> {
  // by default on the start position of Item.
  translateX = new Animated.Value(0);

  _panResponder = PanResponder.create({

    // If a parent View wants to prevent a child View from becoming responder on a move, it should have this handler which returns true.
    onMoveShouldSetResponderCapture: () => true,


    // Should child views be prevented from becoming responder of subsequent touches?
    onMoveShouldSetPanResponderCapture: () => true,

    // - Fired at the end of the touch, ie "touchUp"
    onPanResponderRelease: (e, {vx, dx}) => {
      const screenWidth = Dimensions.get("window").width;

      if (Math.abs(dx) >= 0.3 * screenWidth) {

        Animated.timing(this.translateX, {
          toValue: dx > 0 && 100,
          duration: 200,
          useNativeDriver: true,
        }).start();
      } else {
        Animated.spring(this.translateX, {
          toValue: 0,
          bounciness: 10,
          useNativeDriver: true,
        }).start();
      }
    }
  });

  render() {
    return (
      <Animated.View
          style={[
                 styles.containerItem,
                 {borderColor: this.props.color},
                 {transform: [{translateX: this.translateX}]}
                ]}
                {...this._panResponder.panHandlers}
      >
        <Text>{this.props.title}</Text>
        <Text>{this.props.content}</Text>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 56,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: '#efedf1',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  arrow: {
    color: '#737373'
  },
  containerItem: {
    flex: 1,
    margin: 8,
    borderTopWidth: 3,
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: '#fff',
    minHeight: 55,
    borderRadius: 10,
    ...Platform.select({
      ios: {
        shadowColor: '#555',
        shadowOffset: {  width: 5,  height: 7,  },
        shadowOpacity: 0.5,
        shadowRadius: 6
      },
      android: {
        evolution: 4
      }
    })
  }
});
